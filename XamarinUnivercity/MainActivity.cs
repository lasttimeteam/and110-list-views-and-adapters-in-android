﻿using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using XamarinUniversity;

namespace XamarinUnivercity
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            var instructorList = FindViewById<ListView>(Resource.Id.instructorListView);
            instructorList.ItemClick += OnItemClick;

            instructorList.FastScrollEnabled = true;

            var instructorAdapter = new InstructorAdapter(this, InstructorData.Instructors);
            instructorList.Adapter = instructorAdapter;
        }

        private void OnItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var instructor = InstructorData.Instructors[e.Position];

            var alertDialog = new Android.App.AlertDialog.Builder(this);
            alertDialog.SetMessage(instructor.Name);
            alertDialog.SetPositiveButton("Ok", delegate { });
            alertDialog.Show();
        }
    }
}

